CREATE TABLE IF NOT EXISTS EMPLOYEE
(
    id         INT         NOT NULL,
    name       VARCHAR(50) NOT NULL,
    department VARCHAR(20) NOT NULL
);