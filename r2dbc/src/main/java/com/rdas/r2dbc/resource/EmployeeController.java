package com.rdas.r2dbc.resource;

import com.rdas.r2dbc.model.Employee;
import com.rdas.r2dbc.repository.EmployeeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/employees")
@RequiredArgsConstructor
public class EmployeeController {

    private final EmployeeRepository employeeRepository;

    @PostMapping
    public Mono<Employee> saveEmployee(@RequestBody Employee employee) {
        return employeeRepository.save(employee);
    }

    @GetMapping("/{id}")
    public Mono<Employee> findOne(@PathVariable("id") Long id) {
        return employeeRepository.findById(id);
    }

    @PutMapping
    public Mono<Employee> updateEmployee(@RequestBody Employee employee) {
        return employeeRepository.save(employee);
    }

    @DeleteMapping("/{id}")
    public Mono<Employee> deleteEmployee(@PathVariable("id") Long id) {
        return employeeRepository.findById(id)
                .doOnSuccess(employee -> employeeRepository.delete(employee).subscribe());
    }

    @GetMapping
    public Flux<Employee> findAll() {
        return employeeRepository.findAll();
    }
}
